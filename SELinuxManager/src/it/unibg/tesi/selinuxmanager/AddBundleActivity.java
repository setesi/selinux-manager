package it.unibg.tesi.selinuxmanager;

import java.util.concurrent.ExecutionException;

import it.unibg.tesi.selinuxmanager.utils.ASyncTaskBuildBundle;
import android.app.Activity;
import android.content.Intent;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddBundleActivity extends Activity {
	Button bt_path,bt_ok;
	EditText et_name,et_files_path;
	String path,name;
	
	final static String TAG = "AddBundleActivity";

	private String Path = "";

	//private static final String TAG = "AddBundleActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.add_bundle_activity);
		
		bt_path = (Button)findViewById(R.id.bt_add_path);
		bt_ok = (Button)findViewById(R.id.bt_ok);
		et_name = (EditText)findViewById(R.id.et_name);
		et_files_path = (EditText)findViewById(R.id.et_files_path);
		
		View.OnClickListener clicklistner = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.bt_add_path:
				    
					startActivityForResult(new Intent(AddBundleActivity.this,SelectActivity.class),1);

					break;
				case R.id.bt_ok:
					if(et_files_path.getText().toString().equals("")||et_name.getText().toString().equals(""))
					{
						toast(getResources().getString(R.string.alert_not_null));
						return;
					}
					path = et_files_path.getText().toString();
					name = et_name.getText().toString();
					ASyncTaskBuildBundle at = new ASyncTaskBuildBundle(AddBundleActivity.this,et_name.getText().toString());
					at.execute(path+"/file_contexts",path+"/property_contexts",path+"/sepolicy",path+"/seapp_contexts");
					try {
						at.get();
						setResult(1);
						finish();
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					}
						
					break;

				default:
					break;
				}
				
			}
		};
		
		bt_ok.setOnClickListener(clicklistner);
		bt_path.setOnClickListener(clicklistner);
		
		
		
		super.onCreate(savedInstanceState);
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		  if (requestCode == 1) {

		     if(resultCode == RESULT_OK){      
		         Path=data.getStringExtra("path");
		         et_files_path.setText(Path);
		     }

		  }
		}//onActivityResult
	
	private void toast(String text)
	{
		Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
	}
	

}
