package it.unibg.tesi.selinuxmanager.ui.utils;

public class CustomRow1 {
	
	String title,desc;
	
	public CustomRow1 (String title,String desc){
		this.title=title;
		this.desc=desc;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public String getDescription()
	{
		return desc;
	}
	public void setDescription(String desc)
	{
		this.desc=desc;
	}

}
