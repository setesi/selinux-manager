package it.unibg.tesi.selinuxmanager.ui.utils;

import it.unibg.tesi.selinuxmanager.AddBundleActivity;
import it.unibg.tesi.selinuxmanager.R;
import it.unibg.tesi.selinuxmanager.utils.ASyncTaskReLoadSepolicy;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.google.common.io.Closeables;


public class FileManagerSectionFragment extends Fragment
{
	private static final String TAG = "FileManagerSectionFragment";
	
    // Policy file locations
    private File mBundleFile = null;
    private File mMetadataFile = null;
    private File mZipFile = null;

    // Names of selinux policy bundles/files.
    private final String SEBUNDLE_FILE_PATH = "selinux_bundle";
    private final String SEMETADATA_FILE_PATH = "selinux_bundle_metadata";


    // Names of zip file entries. Derived from buildbundle tool generation.
    private final String ZIP_BUNDLE_ENTRY = "update_bundle";
    private final String ZIP_METADATA_ENTRY = "update_bundle_metadata";

    // Map zip file entries to filesystem names. Useful when unzipping.
    private final Map<String, File> zipMap = new HashMap<String, File>(2);
    
    
    ListView listView;
    private static List<String> itemList;
    private static ArrayAdapter<String> arrayAdapter;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	
    	itemList = new ArrayList<String>();
    	arrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,itemList);
    	super.onCreate(savedInstanceState);
    }
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_filemanager_activity, container, false);
		Button add = (Button)view.findViewById(R.id.bt_add_bundle);
		listView = (ListView) view.findViewById(R.id.listView);
		
		listView.setAdapter(arrayAdapter);
		
		
		View.OnClickListener clicklistener = new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.bt_add_bundle:
					startActivityForResult(new Intent(getActivity(),AddBundleActivity.class),1);
					
					break;

				default:
					break;
				}
				
			}
		};
		
		
		
		OnItemClickListener itemClickListener = new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View view,
					int position, long id) {
				
				TextView tv = (TextView)view;
				String bundle_zip_name = tv.getText().toString();
				
				sendIntentBroadcast(bundle_zip_name);

                
				
			}
		};
		
		OnItemLongClickListener itemLongClickListener = new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view,
					final int position, long id) {
				TextView tv = (TextView)view;
				
				final String bundle_zip_name = tv.getText().toString();
				Builder builder = new Builder(getActivity());
				builder.setIcon(getResources().getDrawable(android.R.drawable.ic_dialog_alert));
				builder.setTitle(getResources().getString(R.string.msg_delete));
				builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						File file = new File(Environment.getExternalStorageDirectory()+"/SEbundles",bundle_zip_name);
						file.delete();
						itemList.remove(position);
						arrayAdapter.notifyDataSetChanged();
						
					}
			
				});
				builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
			
				});
				builder.show();
				return true;
			}
		};
		
		
		add.setOnClickListener(clicklistener);
		listView.setOnItemClickListener(itemClickListener);
		listView.setOnItemLongClickListener(itemLongClickListener);
		
		return view;
	}
	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode==1)
			refreshList();
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	
	
	
	@SuppressWarnings("deprecation")
	private void sendIntentBroadcast(String bundle_zip_name)
	{
		// The zip file entries get written to cache.
        final File cacheDir = new File(Environment.getExternalStorageDirectory()+"/SEbundle_unzip/"+bundle_zip_name.replace(".zip",""));
        final String SystemCacheDir = "cache/SEbundle_unzip/"+bundle_zip_name.replace(".zip","/");

        if(!cacheDir.exists())cacheDir.mkdirs();
            mBundleFile = new File(cacheDir, SEBUNDLE_FILE_PATH);
            mMetadataFile = new File(cacheDir, SEMETADATA_FILE_PATH);
        

        

        // Map zip entries to writable filesystem locations.
        zipMap.put(ZIP_BUNDLE_ENTRY, mBundleFile);
        zipMap.put(ZIP_METADATA_ENTRY, mMetadataFile);
		
		
		
		// The zip file is expected to live on sdcard.
        // This location will likely need to change to a more secure
        // spot when on an actually operational device.
        final File extDir = new File (Environment.getExternalStorageDirectory()+"/SEbundles");
        if (!extDir.exists()) 
        	extDir.mkdir();
        mZipFile = new File(extDir, bundle_zip_name);
            
       
		
		
		
		
		Log.d(TAG, "Loading of policy bundle requested.");

        // Todo: assurance that this preference isn't quickly double clicked.
        // Todo: gurantee that the bundle hasn't been replaced or written
        // to by the time this is called.

        FileInputStream fis = null;
        ZipInputStream zis = null;
        // Maybe put in own thread....
        try {
            fis = new FileInputStream(mZipFile);
            zis = new ZipInputStream(fis);
            ZipEntry ze;
            while ((ze = zis.getNextEntry()) != null) {
                String name = ze.getName();
                if (zipMap.containsKey(name)) {
                    File output = zipMap.get(name);
                    try {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        byte[] data = new byte[1024];
                        int n;
                        while ((n = zis.read(data)) != -1) {
                            baos.write(data, 0, n);
                        }
                        atomicWriteToFile(cacheDir, output, baos.toByteArray());
                    } catch (IOException ioex) {
                    	ioex.printStackTrace();
                        //throw new Exception("Failed extracting policy zip. " + ioex);
                    } finally {
                        zis.closeEntry();
                    }
                }
            }

            // Open the metadata file.
            Scanner scan = new Scanner(mMetadataFile);
            scan.useDelimiter(":");
            String requiredHash = scan.next();
            String signature = scan.next();
            String version = scan.next();
            scan.close();
            
            //prima di inviare il broacast, copiamo i SEbundle files dalla sdcard a /cache/SEbundles_unzip
            SEbundleFilesToCache(bundle_zip_name);
            
            // Build the intent to broadcast.
            Intent i = new Intent("android.intent.action.UPDATE_SEPOLICY");
          
            //i.putExtra("CONTENT_PATH", "/cache/"+SEBUNDLE_FILE_PATH);
            i.putExtra("CONTENT_PATH", SystemCacheDir+SEBUNDLE_FILE_PATH);
            i.putExtra("REQUIRED_HASH", requiredHash);
            i.putExtra("SIGNATURE", signature);
            i.putExtra("VERSION", version);

            Log.d(TAG, "UPDATE_SEPOLICY intent being broadcast.");
            getActivity().sendBroadcast(i);

        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.toString(), Toast.LENGTH_SHORT).show();
            mBundleFile.delete();
            mMetadataFile.delete();
            Log.e(TAG, "Exception loading policy.", ex);
        } finally {
        	if (zis != null) Closeables.closeQuietly(zis);
            if (fis != null) Closeables.closeQuietly(fis);
            Toast.makeText(getActivity(), getResources().getString(R.string.success_upd_sp), Toast.LENGTH_LONG).show();
            new ASyncTaskReLoadSepolicy().execute();
        }
	}
	
    private void SEbundleFilesToCache(String bundleFolder) {
    	
    	Log.i(TAG, "Trying to copy sebundle files from sdcard to /cache");
    	try
    	{
    	String comando1 = "mount -orw,remount /";
    	String comando2 = "cp -r storage/sdcard0/SEbundle_unzip cache";
    	String comando3 = "chmod 777 cache/SEbundle_unzip";
    	String comando4 = "chmod 777 cache/SEbundle_unzip/*";
    	String comando5 = "chmod 777 cache/SEbundle_unzip/"+bundleFolder.replace(".zip", "")+"/*";

    	Process suProcess = Runtime.getRuntime().exec("su");
    	DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());
    	os.writeBytes(comando1 + "\n");
    	os.flush();
    	os.writeBytes(comando2 + "\n");
    	os.flush();
    	os.writeBytes(comando3 + "\n");
    	os.flush();
    	os.writeBytes(comando4 + "\n");
    	os.flush();
    	os.writeBytes(comando5 + "\n");
    	os.flush();
    	os.writeBytes("exit\n");
    	os.flush();
    	suProcess.waitFor();
    	Log.i(TAG, "Success copying sebundle files from sdcard to /cache");
    	}
    	catch (Exception ex)
    	{
    	 Log.w("Error copying sebundle files to /cache", ex);
    	}
		
	}//SEbundleFilesToCache
    
    public void refreshList()
    {
    	Log.i(TAG, "refreshing list");
    	String[] mFileList = loadFileList();
    	itemList.clear();
        
        for (int i=0;i<mFileList.length;i++)
        {
        	itemList.add(mFileList[i]);
        }
        arrayAdapter.notifyDataSetChanged();
        
    }
    
    private String[] loadFileList() {
    	File mPath = new File(Environment.getExternalStorageDirectory().getPath()+"/SEbundles");
	    String[] mFileList;
    	try {
	        mPath.mkdirs();
	    }
	    catch(SecurityException e) {
	        Log.e(TAG, "unable to write on the sd card " + e.toString());
	    }
	    if(mPath.exists()) {
	        FilenameFilter filter = new FilenameFilter() {
	            public boolean accept(File dir, String filename) {
	                File sel = new File(dir, filename);
	                return  sel.isFile();
	            }
	        };
	        mFileList = mPath.list(filter);
	        
	        return mFileList;
	    }
	    else {
	        mFileList= new String[0];
	        
	        return mFileList;
	    }
	}
    
    
    @SuppressWarnings("deprecation")
	private void atomicWriteToFile(File dir, File file, byte[] content) throws IOException {
        FileOutputStream out = null;
        File tmp = null;
        try {
            tmp = File.createTempFile("journal", "", dir);
            tmp.setReadable(true, false);
            out = new FileOutputStream(tmp);
            out.write(content);
            out.getFD().sync();
            if (!tmp.renameTo(file)) {
                throw new IOException("Failed to atomically rename " + file.getCanonicalPath());
            }
        } finally {
            if (tmp != null) {
                tmp.delete();
            }
            Closeables.closeQuietly(out);
        }
    }
	
    
    
}