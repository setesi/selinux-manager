package it.unibg.tesi.selinuxmanager.ui.utils;


import it.unibg.tesi.selinuxmanager.R;
import java.util.List;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
 
public class CustomListAdapter1 extends ArrayAdapter<CustomRow1> {
 
    Context context;
    SharedPreferences sp;
    int resourceId;
    boolean withDesc;
 
    public CustomListAdapter1(Context context, int resourceId,
            List<CustomRow1> items,boolean withDesc) {
        super(context, resourceId, items);
        this.context = context;
        this.resourceId=resourceId;
        this.withDesc=withDesc;
		this.sp = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
    }
 
    /*private view holder class*/
    private class ViewHolder {
        TextView title;
        TextView desc;
    }
 
    public synchronized View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        CustomRow1 rowItem = getItem(position);
 
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
        	convertView = mInflater.inflate(resourceId, null);
        		
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tv_title);
            holder.desc = (TextView) convertView.findViewById(R.id.tv_desc);
            
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
       
        
        
        holder.title.setText(rowItem.getTitle());	
        holder.desc.setText(rowItem.getDescription());
        holder.desc.setVisibility(withDesc ? View.VISIBLE : View.GONE);
        return convertView;
    }
}

