package it.unibg.tesi.selinuxmanager.utils;

import it.unibg.tesi.selinuxmanager.MainActivity;
import it.unibg.tesi.selinuxmanager.R;

import java.io.DataOutputStream;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

public class ASyncTaskFirstConfig extends AsyncTask<String, String, Boolean> {
	
	public static final String TAG = "ASyncTaskFirstConfig";
	private ProgressDialog dialog;
	private Context context;
	
	/**
	 * This will insert the certificate, used to sign the bundle, to system/settings database.
	 * 
	 * It will only run once.
	 * @param context - The context
	 */
	public ASyncTaskFirstConfig(Context context)
	{
		this.context=context;
	}
	
	@Override
	protected void onPreExecute() {
		dialog =  new ProgressDialog(context);
		dialog.setTitle(context.getResources().getString(R.string.checking_4_root));
		dialog.show();
		super.onPreExecute();
	}
	
	@Override
	protected Boolean doInBackground(String... params) {
		
		SharedPreferences sp = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
		try
		{
			Process suProcess = Runtime.getRuntime().exec("su");
			publishProgress(context.getResources().getString(R.string.inserting_certificate));
			DataOutputStream os = null;
    		
    		if (!sp.getBoolean("copyCertificate", false))
    		{
    			Log.i(TAG,"Inserting Certificate into Settings");
        		Editor editor = sp.edit();
        		
            	String comando1 = "cd /data/data/com.android.providers.settings/databases";
            	String comando2 = "sqlite3 settings.db \"INSERT into secure (name, value) VALUES('config_update_certificate','" +
            			"MIIEqDCCA5CgAwIBAgIJAJNurL4H8gHfMA0GCSqGSIb3DQEBBQUAMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTAeFw0wODAyMjkwMTMzNDZaFw0zNTA3MTcwMTMzNDZaMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbTCCASAwDQYJKoZIhvcNAQEBBQADggENADCCAQgCggEBANaTGQTexgskse3HYuDZ2CU+Ps1s6x3i/waMqOi8qM1r03hupwqnbOYOuw+ZNVn/2T53qUPn6D1LZLjk/qLT5lbx4meoG7+yMLV4wgRDvkxyGLhG9SEVhvA4oU6Jwr44f46+z4/Kw9oe4zDJ6pPQp8PcSvNQIg1QCAcy4ICXF+5qBTNZ5qaU7Cyz8oSgpGbIepTYOzEJOmc3Li9kEsBubULxWBjf/gOBzAzURNps3cO4JFgZSAGzJWQTT7/emMkod0jb9WdqVA2BVMi7yge54kdVMxHEa5r3b97szI5p58ii0I54JiCUP5lyfTwE/nKZHZnfm644oLIXf6MdW2r+6R8CAQOjgfwwgfkwHQYDVR0OBBYEFEhZAFY9JyxGrhGGBaR0GawJyowRMIHJBgNVHSMEgcEwgb6AFEhZAFY9JyxGrhGGBaR0GawJyowRoYGapIGXMIGUMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEQMA4GA1UEChMHQW5kcm9pZDEQMA4GA1UECxMHQW5kcm9pZDEQMA4GA1UEAxMHQW5kcm9pZDEiMCAGCSqGSIb3DQEJARYTYW5kcm9pZEBhbmRyb2lkLmNvbYIJAJNurL4H8gHfMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADggEBAHqvlozrUMRBBVEY0NqrrwFbinZaJ6cVosK0TyIUFf/azgMJWr+kLfcHCHJsIGnlw27drgQAvilFLAhLwn62oX6snb4YLCBOsVMR9FXYJLZW2+TcIkCRLXWG/oiVHQGo/rWuWkJgU134NDEFJCJGjDbiLCpe+ZTWHdcwauTJ9pUbo8EvHRkU3cYfGmLaLfgn9gP+pWA7LFQNvXwBnDa6sppCccEX31I828XzgXpJ4O+mDL1/dBd+ek8ZPUP0IgdyZm5MTYPhvVqGCHzzTy3sIeJFymwrsBbmg2OAUNLEMO6nwmocSdN2ClirfxqCzJOLSDE4QyS9BAH6EhY6UFcOaE0=" +
            			"');\"";
            	
            	
            	os = new DataOutputStream(suProcess.getOutputStream());
            	os.writeBytes(comando1 + "\n");
            	os.flush();
            	os.writeBytes(comando2 + "\n");
            	os.flush();
            	os.writeBytes("exit\n");
    			os.flush();
    			suProcess.waitFor();
            	
            	editor.putBoolean("copyCertificate", true);
            	editor.commit();
            	
        	}
        	else
        		Log.i(TAG, "Certificate already inserted");

    		return true;
		}
		catch (Exception ex){
			Log.w("Error inserting certificate to system/settings", ex);
		    return false;
		}
			
			
		

		
	}//DoitInBackground
	
	@Override
	protected void onProgressUpdate(String... values) {
		dialog.setTitle(values[0]);
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		dialog.dismiss();
		if(result)
		{	
			context.startActivity(new Intent(context,MainActivity.class));
			((Activity)context).finish();
		}
		else
			toast(context.getResources().getString(R.string.error_phone_rooted),Toast.LENGTH_LONG);
		
		super.onPostExecute(result);
	}
	
	private void toast(String message,int duration)
    {
    	Toast.makeText(context, message, duration).show();
    }

}// Class ASyncTaksFirstConfig