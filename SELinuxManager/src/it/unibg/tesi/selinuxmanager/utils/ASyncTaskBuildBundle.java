package it.unibg.tesi.selinuxmanager.utils;

import it.unibg.tesi.selinuxmanager.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.google.common.base.Joiner;
import com.google.common.io.ByteStreams;
import com.google.common.io.Files;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.widget.Toast;

public class ASyncTaskBuildBundle extends AsyncTask<String, Void, Boolean>{
	
	 
     String version = "1";
     String requiredHash = "NONE";
     String outputName = "update_bundle.zip";
     ArrayList<String> configPaths;
     Context context;
     File output;
     ProgressDialog dialog;
	/**
	 * @param context context of application
	 * @param privateKey path of the privateKey
	 * @param version path
	 * @param requiredHash path
	 * @param outputName path
	 * 
	 * doInBackground(String... params)
	 */
	public ASyncTaskBuildBundle (Context context,String version,String requiredHash,String outputName)
	{
			this.context=context;
	        this.version=version;
	        this.requiredHash = requiredHash;
	        this.outputName = outputName;
	        
	        configPaths = new ArrayList<String>();
	}
	
	public ASyncTaskBuildBundle (Context context)
	{
			this.context=context;
	        configPaths = new ArrayList<String>();
	}
	/**
	 * Create the singed bundle from the 4 files passed on exec()
	 * 
	 * @param context the context
	 * @param outputName the name of the output zip, without the extension (.zip)
	 */
	public ASyncTaskBuildBundle (Context context,String outputName)
	{
			this.context=context;
			this.outputName=outputName+".zip";
	        configPaths = new ArrayList<String>();
	}
	
	@Override
	protected void onPreExecute() {
		dialog = new ProgressDialog(context);
		dialog.setTitle("Creating Bundle..");
		dialog.show();
		super.onPreExecute();
	}
	
	/**
	 * 
	 * @param params String paths of the 4 files in this order : file_contexts, property_contexts, sepolicy, seapp_contexts
	 * 				
	 */
	@Override
	protected Boolean doInBackground(String... params) {
		File dir = new File( Environment.getExternalStorageDirectory()+"/SEbundles");
		if (!dir.exists())dir.mkdir();
	
		output = new File(dir, outputName);
		
		
		for(int i=0;i<params.length;i++)
		{
			configPaths.add(params[i]);
		}
		
		try {
            byte[] bundle = build_bundle(configPaths);
            byte[] signed = sign_bundle(bundle, version, requiredHash);

            String joined = Joiner.on(":").join(requiredHash, new String(signed), version);
            byte[] joined_bytes = joined.getBytes();

            // Build zip file
            final ZipOutputStream out = new ZipOutputStream(new FileOutputStream(output));
            ZipEntry e = new ZipEntry("update_bundle");
            out.putNextEntry(e);
            out.write(bundle, 0, bundle.length);
            out.closeEntry();
            e = new ZipEntry("update_bundle_metadata");
            out.putNextEntry(e);
            out.write(joined_bytes, 0, joined_bytes.length);
            out.closeEntry();
            out.close();
        } catch (IOException ioex) {
           ioex.printStackTrace();
           return false;
        } catch (GeneralSecurityException gex) {
        	gex.printStackTrace();
        	return false;
        }
		return true;
	}
	
	@Override
	protected void onPostExecute(Boolean result) {
		dialog.dismiss();
		if(result)
			toast(context.getResources().getString(R.string.success_creating_sebundle),Toast.LENGTH_LONG);
		else
			toast(context.getResources().getString(R.string.error_creating_sebundle),Toast.LENGTH_LONG);
		
		super.onPostExecute(result);
	}

	/**
     * Given a File object encode it according to the
     * current bundle scheme used by ConfigUpdateInstallReceiver.
     * Presently, the encoding is base64 chunked, line
     * wrapped at 76 characters, with each line ending
     * in '\r\n'. A byte array of the encoded file is returned.
     * Base64 is the choosen encoding scheme for OTA config
     * bundle updates and this function is subject to change
     * if the update mechanism itself changes.
     *
     * @param path File object of the file to encode
     *
     * @exception IOException produced by failed or interrupted
     *            I/O operations on the requested path
     *
     * @return byte array of the encoded file scheme
     * @hide
     */
	private  byte[] create_encoding(File path) throws IOException {

        if (path == null) {
            throw new IOException("Requested path is null.");
        }

        byte[] policy = Files.toByteArray(path);
        return Base64.encode(policy, Base64.DEFAULT);
    }
	
	/**
     * Given an array of paths to files, create a bundle
     * capable of being loaded via the ConfigUpdateInstallReceiver
     * mechanism. The order of the entries in the array will
     * be preserved when building the bundle. The bundle as
     * a byte array is returned as is capable of being directly
     * loaded via the ConfigUpdateInstallReceiver mechanism.
     * If there are no paths passed then no bundle is created;
     * however, an empty byte array will still be returned.
     * No metadata about the bundle is returned; additional
     * processing must be performed to calculate that data.
     *
     * @param paths ArrayList of strings representing paths
     *              to config files to include in the bundle
     *
     * @exception IOException produced by failed or interrupted
     *            I/O operations on any of the requested paths
     *
     * @return byte array of the created config bundle
     */
    public  byte[] build_bundle(ArrayList<String> paths) throws IOException {

        if (paths == null) {
            throw new IOException("Requested paths is null.");
        }

        int numOfPaths = paths.size();
        int[] lengths = new int[numOfPaths];
        byte[][] files = new byte[numOfPaths][];

        for (int i = 0; i < numOfPaths; i++) {
            files[i] = create_encoding(new File(paths.get(i)));
            lengths[i] = files[i].length;
        }

        ByteBuffer b = ByteBuffer.allocate(numOfPaths * 4);
        for (int i = 0; i < numOfPaths; i++) {
            b.putInt(lengths[i]);
        }

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        output.write(b.array());
        for (int i = 0; i < numOfPaths; i++) {
            output.write(files[i]);
        }

        return output.toByteArray();
    }
    
   
    
   
    
    /**
     * Return a PrivateKey object of the private key after being
     * decrypted. The private key is
     * assumed to be encoded according to the pkcs8 standard.
     *
     * @param privateKey the private key to decrypt given as byte array
     *
     * @exception IOException produced by failed or interrupted I/O
     *            operations when retrieving the password for the key
     * @exception GeneralSecurityException generic security exceptions
     *            that result from signature and key operations.
     *
     * @return a PrivateKey interface object to the underlying
     *         key material
     */
    private  PrivateKey getPrivateKey(byte[] privateKey)
            throws IOException, GeneralSecurityException {

        
        
         KeySpec spec = new PKCS8EncodedKeySpec(privateKey);
        

        try {
            return KeyFactory.getInstance("RSA").generatePrivate(spec);
        } catch (InvalidKeySpecException ex) {
            System.err.println("Key.pk8 probably not a PKCS#8 DER formatted RSA cert.");
            throw new GeneralSecurityException(ex);
        }
    }
    
    /**
     * Takes a byte array as well as the version and previous hash and
     * computes the digital signature using sha512 and RSA. The secured
     * message is then returned as a byte array. Uses the private key.pk8 locate in /assests
     *
     * @param bundle byte array representing the built config bundle
     * @param version the version of this config update
     * @param requiredHash the hash of the previous config update
     *                     that will be replaced
     *
     * @exception IOException produced by failed or interrupted
     *            I/O operations on the subprocess
     * @exception GeneralSecurityException generic security exceptions
     *            that result from signature and hashing attempts
     *
     * @return a byte array of the signed message
     */
    public  byte[] sign_bundle(byte[] bundle, String version, String requiredHash)
            throws IOException, GeneralSecurityException {
    	
    	InputStream is = context.getAssets().open("key.pk8");
       // InputStream is = new FileInputStream(new File (privKey));
        
        byte[] privateKey = ByteStreams.toByteArray(is);
        is.close();
        PrivateKey pk = getPrivateKey(privateKey);

        Signature signer = Signature.getInstance("SHA512withRSA");
        signer.initSign(pk);
        signer.update(bundle);
        signer.update(version.getBytes());
        signer.update(requiredHash.getBytes());
        // The signature should be one large string
        return Base64.encode(signer.sign(), Base64.NO_WRAP);
    }
    
    public void toast(String message,int duration)
    {
    	Toast.makeText(context, message, duration).show();
    }
	
}
