package it.unibg.tesi.selinuxmanager.utils;

import it.unibg.tesi.selinuxmanager.R;

import java.io.DataOutputStream;
import java.io.IOException;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class ASyncTaskRestoreDefaultSepolicy extends AsyncTask<String, Void, Boolean>{
	
	ProgressDialog dialog;
	Context context;
	String msg;
	
	/**
	 * This will restore the default sepolicy by deleting all files on the directory /data/security/.
	 * @param context - The context
	 */
	public ASyncTaskRestoreDefaultSepolicy (Context context)
	{
		this.context=context;
		msg=context.getResources().getString(R.string.restoring_sepolicy);
	}
	
	@Override
	protected void onPreExecute() {
		dialog = new ProgressDialog(context);
		dialog.setTitle(msg);
		dialog.show();
		super.onPreExecute();
	}
	
	@Override
	protected Boolean doInBackground(String... params) {
		try {
			Process suProcess = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());
			String command1 = "rm -r data/security/*";
	
			os.writeBytes(command1 + "\n");
			os.flush();
			os.writeBytes("exit\n");
			os.flush();
			suProcess.waitFor();
			
			
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			} catch (InterruptedException e) {
				e.printStackTrace();
				return false;
			}
		return true;
	}
	@Override
	protected void onPostExecute(Boolean result) {
		toast(result ? "Success "+msg:"Error "+msg,Toast.LENGTH_LONG);
		dialog.dismiss();
		super.onPostExecute(result);
	}
	
	private void toast(String message,int duration)
    {
    	Toast.makeText(context, message, duration).show();
    }

}
