package it.unibg.tesi.selinuxmanager.utils;

import java.io.DataOutputStream;
import java.io.IOException;

import android.os.AsyncTask;
import android.util.Log;

public class ASyncTaskReLoadSepolicy extends AsyncTask<String, Void, Boolean>{
	
	private static final String TAG = "ASyncTaskReLoadSepolicy";
	
	@Override
	protected Boolean doInBackground(String... params) {
		
		try {
			Log.i(TAG, "Reloading sepolicy");
			Process suProcess = Runtime.getRuntime().exec("su");
			DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());
			String command1 = "setprop selinux.reload_policy 1";
	
			os.writeBytes(command1 + "\n");
			os.flush();
			os.writeBytes("exit\n");
			os.flush();
			suProcess.waitFor();
			
			Log.i(TAG, "Success reloading sepolicy");
			} catch (IOException e) {
				Log.i(TAG, "Error reloading sepolicy");
				e.printStackTrace();
				return false;
			} catch (InterruptedException e) {
				Log.i(TAG, "Error reloading sepolicy");
				e.printStackTrace();
				return false;
			}
		
		return true;
	}

}
