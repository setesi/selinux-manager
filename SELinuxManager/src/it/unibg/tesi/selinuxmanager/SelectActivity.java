package it.unibg.tesi.selinuxmanager;

import it.unibg.tesi.selinuxmanager.ui.utils.CustomListAdapter1;
import it.unibg.tesi.selinuxmanager.ui.utils.CustomRow1;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class SelectActivity extends Activity{
	
	TextView pathBar;
	ListView listview;
	Button btn_ok;
	//private static final String TAG = "SelectActivity";
	String Path="";
	ArrayList<String> prevPath;
	File mPath = Environment.getExternalStorageDirectory();
	List<CustomRow1> list;
	CustomListAdapter1 adapter;
	int c=0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.select_layout);
		
		pathBar = (TextView)findViewById(R.id.textView1);
		listview = (ListView)findViewById(R.id.listView);
		btn_ok = (Button)findViewById(R.id.btn_ok);
		Path = mPath.getAbsolutePath();
		list = new ArrayList<CustomRow1>();
		prevPath = new ArrayList<String>();
		prevPath.add(Path);
		adapter = new CustomListAdapter1(this, R.layout.custom_row_1, list,false);
		listview.setAdapter(adapter);
		
		loadFileList();
		
		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int posi,
					long id) {
				String mChosenFile = list.get(posi).getTitle();
				if(!mChosenFile.equals(".."))
				{	
					c++;
	                Path=Path+"/"+mChosenFile;
	                prevPath.add(c,Path);
				}
				else
				{
					c--;
					Path=prevPath.get(c);
				}
                loadFileList();
			}});
		btn_ok.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				 Intent returnIntent = new Intent();
				 returnIntent.putExtra("path",Path);
				 setResult(RESULT_OK,returnIntent);     
				 finish();
				
			}
		});
		
	}
	
	
	
	private void loadFileList() {
	    File dir = new File(Path);
	    pathBar.setText(Path);
	    String[] mFileList;
	    if(dir.exists()) {
	        FilenameFilter filter = new FilenameFilter() {
	            public boolean accept(File dir, String filename) {
	                File sel = new File(dir, filename);
	                return sel.isDirectory();
	            }
	        };
	        mFileList = dir.list(filter);
	    }
	    else {
	        mFileList= new String[0];
	    }
	    list.clear();
	    for(int i=0;i<mFileList.length;i++)
	    {
	    	list.add(new CustomRow1(mFileList[i], ""));
	    }
	    if(!Path.equals(mPath.getAbsolutePath()))
	    	list.add(0,new CustomRow1("..",""));
	    adapter.notifyDataSetChanged();
	}

}
