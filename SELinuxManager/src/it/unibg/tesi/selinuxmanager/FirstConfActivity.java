package it.unibg.tesi.selinuxmanager;

import it.unibg.tesi.selinuxmanager.utils.ASyncTaskFirstConfig;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;

public class FirstConfActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.firstconf_activity);
		
		SharedPreferences sp = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
		if (!sp.getBoolean("firstConf", false))
			{
				Editor editor = sp.edit();
				new ASyncTaskFirstConfig(this).execute();
				editor.putBoolean("firstConf", true);
				editor.commit();
			}
		else
			{
				startActivity(new Intent(this,MainActivity.class));
				finish();
			}
	}

	
	
	
}
