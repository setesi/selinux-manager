package it.unibg.tesi.selinuxmanager;

import it.unibg.tesi.selinuxmanager.ui.utils.CustomListAdapter1;
import it.unibg.tesi.selinuxmanager.ui.utils.CustomRow1;
import it.unibg.tesi.selinuxmanager.ui.utils.FileManagerSectionFragment;
import it.unibg.tesi.selinuxmanager.utils.ASyncTaskRestoreDefaultSepolicy;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import android.app.ActionBar;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.AdapterView.OnItemClickListener;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

	//private static String MAIN_TAG = "MainActivity";

    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    static ViewPager mViewPager;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        final ActionBar actionBar = getActionBar();
        
        
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

     
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.pager);
        
        mViewPager.setAdapter(mSectionsPagerAdapter);

        
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
                if(position==1){
                	FileManagerSectionFragment fmsm = (FileManagerSectionFragment)mSectionsPagerAdapter.getItem(position);
                	fmsm.refreshList();
                }
                
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
   @Override
   	public boolean onOptionsItemSelected(MenuItem item) {
	   switch (item.getItemId()) {
	case R.id.action_info:
					final Dialog dialog = new Dialog(MainActivity.this);
					dialog.setContentView(R.layout.info_layout);
					dialog.setTitle("Info");
					Button dialogButton = (Button) dialog.findViewById(R.id.btn_ok);
					dialogButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
						}
					});
		 
					dialog.show();
		return true;

	default:
		return super.onOptionsItemSelected(item);
	}
	   	
   	}
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {

        	Fragment fragment = null;
        	switch (position) {
			case 0:
				fragment = new MainSectionFragment();
				break;

			case 1:
				fragment = new FileManagerSectionFragment();
				break;
			}
            
            return fragment;
        }
        
        

        @Override
        public int getCount() {
            // Show 2 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                
            }
            return null;
        }
    }//sectionPagerAdapter

    public static class MainSectionFragment extends Fragment {
    	
    	Switch setEnforce;
    	
    	
    	@Override
    	public View onCreateView(LayoutInflater inflater, ViewGroup container,
    			Bundle savedInstanceState) {
    		View view = inflater.inflate(R.layout.fragment_main_activity, container, false);
    		ListView listView = (ListView) view.findViewById(R.id.listView);
    		setEnforce = (Switch)view.findViewById(R.id.sw_setenforce);
    		
    		List<CustomRow1> list = new ArrayList<CustomRow1>();
    		
    		//TODO usare @string resource per il titolo e la descrizione.
    		
    		list.add(new CustomRow1(getStringfromRes(R.string.selinux_status), getSelinuxStatus()));
    		list.add(new CustomRow1(getStringfromRes(R.string.load_selinux), getStringfromRes(R.string.load_selinux_desc)));
    		list.add(new CustomRow1(getStringfromRes(R.string.restore_selinux), getStringfromRes(R.string.restore_selinux_desc)));
    		
    		final CustomListAdapter1 customAdapter = new CustomListAdapter1(getActivity(), R.layout.custom_row_1, list,true);
    		listView.setAdapter(customAdapter);
    		
    		OnItemClickListener itemClickListener = new OnItemClickListener() {

    			@Override
    			public void onItemClick(AdapterView<?> adapter, View view,
    					int posi, long id) {
    				
    				switch (posi) {
    				case 1:
    					mViewPager.setCurrentItem(2);
    				break;
    				case 2:
    					new ASyncTaskRestoreDefaultSepolicy(getActivity()).execute();
    				break;

    				default:
    					break;
    				}
    				
    				
    			}
    		};
    		listView.setOnItemClickListener(itemClickListener);
    		
    		setEnforce.setOnCheckedChangeListener(new OnCheckedChangeListener() {
    			
    			@Override
    			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
    				
    				
    				try {
    					Process suProcess = Runtime.getRuntime().exec("su");
    					DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());
    					String command1 = "";
    					if(isChecked)
    						command1 = "setenforce 1";
    					else
    						command1 = "setenforce 0";
    		        	
    					
    					os.writeBytes(command1 + "\n");
    					os.flush();
    					os.writeBytes("exit\n");
    					os.flush();
    					
    					CustomRow1 cr1 = (CustomRow1)customAdapter.getItem(0);
    					cr1.setDescription(getSelinuxStatus());
    					customAdapter.notifyDataSetChanged();
    					
    					} catch (IOException e) {
    						
    						e.printStackTrace();
    					}
    				
    				
    			}
    		});
    		
    		
    		return view;
    	}
    	
    	private String getStringfromRes(int id)
    	{
    		return getResources().getString(id);
    	}
    	
    	public String getSelinuxStatus()
        {
    		String error_msg=getStringfromRes(R.string.error_1);
    		try {
	    		Process	suProcess = Runtime.getRuntime().exec("su");
	    		
	        	DataOutputStream os = new DataOutputStream(suProcess.getOutputStream());
	        	BufferedReader in = new BufferedReader(new InputStreamReader(suProcess.getInputStream()));  
	        	String status="";
	        	String command1 = "getenforce";
	        	
	    			os.writeBytes(command1 + "\n");
	    			os.flush();
	    			os.writeBytes("exit\n");
	    			os.flush();
	    			suProcess.waitFor();
	    			String line = null;  
	    		while ((line = in.readLine()) != null)
	    		        status+=line;  
	    		if(status.equals("Enforcing"))
	    		    	setEnforce.setChecked(true);
	    		else if(status.equals("Permissive"))
	    		    	setEnforce.setChecked(false);
	    		else 
	    				status = error_msg;
	    		return status;
	    		
    		} catch (IOException e) {
    			e.printStackTrace();
    			return error_msg;
    		} catch (InterruptedException e) {
    			e.printStackTrace();
    			return error_msg;
    		}
    	
        	
        }
    	
    }//MainSectionFragment   
}
